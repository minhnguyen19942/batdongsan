<?php
/*
 Plugin Name: Meta Box Post Detail
 Author: Minh Minh ^_^ 
 Description: Tạo meta box ...
 Author URI: #
*/

/**
 Khai báo meta box
**/
function minhnguyen_meta_box()
{
 add_meta_box( 'price-detail', 'Gía', 'minhnguyen_price_detail_output', 'post' );
  add_meta_box( 'tong-quan', 'Tổng Quan', 'minhnguyen_first_detail_output', 'post' );
}
add_action( 'add_meta_boxes', 'minhnguyen_meta_box' );
 
/**
 Khai báo callback
 @param $post là đối tượng WP_Post để nhận thông tin của post
**/
function minhnguyen_price_detail_output( $post )
{
 $price_detail = get_post_meta( $post->ID, '_price_detail', true );
 // Tạo trường Link Download
 echo ( '<label for="price_detail">Giá Sản Phẩm: </label>' );
 echo ('<input type="text" id="price_detail" name="price_detail" value="'.esc_attr( $price_detail ).'" />');
}
function minhnguyen_first_detail_output( $post )
{
 $first_detail = get_post_meta( $post->ID, '_first_detail', true );
 // Tạo trường Link Download
 echo ( '<label for="first_detail">Chi tiết sản phẩm </label>' );
 echo ('<textarea id="first_detail" name="first_detail" />'.esc_attr( $first_detail ).'</textarea>');
}
 
/**
 Lưu dữ liệu meta box khi nhập vào
 @param post_id là ID của post hiện tại
**/
function minhnguyen_thongtin_save( $post_id )
{
 $price_detail = sanitize_text_field( $_POST['price_detail'] );
 update_post_meta( $post_id, '_price_detail', $price_detail );

  $first_detail = sanitize_text_field( $_POST['first_detail'] );
 update_post_meta( $post_id, '_first_detail', $first_detail );
}
add_action( 'save_post', 'minhnguyen_thongtin_save' );