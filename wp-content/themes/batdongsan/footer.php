<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

</div>
<footer class="footer">
    <div class="brand-footer">
      <div class="container">
        <div class="slider-footer"></div>
      </div>
    </div>
    <div class="bottom-footer row">
      <div class="container">
        <div class="row">
          <div class="bottom-footer__info col-sm-4 col-md-4 col-lg-4">
          	<?php 
      				if(is_active_sidebar("footer-1")){
      					dynamic_sidebar("footer-1");
      				}
      			?>
          </div>
          <div class="bottom-footer__contact col-sm-8 col-md-8 col-lg-8">
            <?php 
      				if(is_active_sidebar("footer-2")){
      					dynamic_sidebar("footer-2");
      				}
      			?>
          </div>
        </div>
      </div>
    </div>
  </footer>


<?php wp_footer(); ?>
</body>
</html>
