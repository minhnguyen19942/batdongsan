<div class="header-single-post row">

	<div class="col-sm-8 col-md-8 col-lg-8">
		<div class="demo">
		    <ul id="lightSlider">
		    	<li data-thumb=" <?php echo get_the_post_thumbnail_url();?>">
		            <?php the_post_thumbnail(); ?>
		        </li>
		        <?php 
		        	for ($i=0; $i < 10; $i++) { 
		        		$image_name = 'featured-image-' . $i;
		        ?>

		        <li data-thumb="<?php  echo kdmfi_get_featured_image_src( $image_name, 'full' ); ?>">
		           <img src="<?php  echo kdmfi_get_featured_image_src( $image_name, 'full' ); ?>" />
		        </li>

		       	<?php 
		       		}
		       	?>
		    </ul>
		</div>
	</div>
	<div class="col-sm-4 col-md-4 col-lg-4">
		<?php echo get_post_meta( $post->ID, '_price_detail', true );?>
		<?php echo get_post_meta( $post->ID, '_first_detail', true );?>
	</div>
</div>
<div class="detail-single-post row">
	<ul class="menu-detail-single-post"> 
		<li><a href="#tab01">TỔNG QUAN</a></li>
		<li><a href="#tab02">VỊ TRÍ</a></li>
		<li><a href="#tab03">TIỆN ÍCH</a></li>
		<li><a href="#tab04">MẶT BẰNG</a></li>
		<li><a href="#tab05">CHÍNH SÁCH BÁN HÀNG</a></li>
		<li><a href="#tab06">LÝ DO</a></li>
		<li><a href="#tab07">LỰA CHỌN</a></li>
	</ul>
	<div class="content-detail-single-post">
		<?php the_content();?>
	</div>
</div>