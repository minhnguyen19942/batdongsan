<?php
/**
*  Advance Search Widget
*/
class Advance_Search_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct(
			'advance_search_widget',
			'Widget Advance Search',

			array(
				'description' => 'Widget Advance Search By Taxanomy'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'title' => 'Advance Search Widget'
		);
	}

	function update($new_instance, $old_instance){

	}
	function widget($args,$instance){
		extract( $args );
 
        echo $before_widget;
 
        //In tiêu đề widget
        //echo $before_title.$title.$after_title;
 
        // Nội dung trong widget
 	?>
<script type="text/javascript">	

	function show_advanced_search(select,text,option,id_input,speedshow){
		text.click(function(e){
			if(select.hasClass("active")){
				select.removeClass("active");
				select.slideUp(speedshow);
			} else {
				select.addClass("active");
				$(".select-form .select").slideUp(200);
				select.slideDown(200);
			}
		});
		option.click(function(e){
			valueOption = $(this).attr("value");
			htmlOption =  $(this).html();
			id_input.val(valueOption);
			text.html(htmlOption);
			select.removeClass("active");
			select.slideUp(200);
		});
	}


	$(document).ready(function($){
		area_text = $(".area-search .text");
		area_select = $(".area-search .select");
		area_option = $(".area-search .option");
		area_id_input = $("#area_search");
		area_speedshow = 200;
		show_advanced_search(area_select,area_text,area_option,area_id_input,area_speedshow);

		level_text = $(".level .text");
		level_select = $(".level .select");
		level_option = $(".level .option");
		level_id_input = $("#level");
		level_speedshow = 200;
		show_advanced_search(level_select,level_text,level_option,level_id_input,level_speedshow);

		career_text = $(".career .text");
		career_select = $(".career .select");
		career_option = $(".career .option");
		career_id_input = $("#career");
		career_speedshow = 200;
		show_advanced_search(career_select,career_text,career_option,career_id_input,career_speedshow);

		$(".select-form .get_value").click(function(e){
			e.stopPropagation();
		});
	});


	$(document).click(function(){
		area_select = $(".area-search .select");
		level_select = $(".level .select");
		career_select = $(".career .select");
		if($(".select-form .select").hasClass("active")){
			area_select.removeClass("active");
			area_select.slideUp(200);
	    	level_select.removeClass("active");
			level_select.slideUp(200);
	    	career_select.removeClass("active");
			career_select.slideUp(200);
	    }
	});
	
</script>

 	<div class="advanced-search">
        <form action="<?php echo esc_url( 'http://gochamhoc.dev/advance-search/' ); ?>" method="GET">
			<h3>TÌM KIẾM NÂNG CAO</h3>
			<div class="area-search select-form">
				<div class="title">Khu vực</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="text" value="">-- Chọn khu vực --</div>
				</div>
				<div class="select">
				<?php
					$terms = get_terms( array(
					    'taxonomy' => 'khu-vuc',
					    'hide_empty' => false,
					    'parent' => 0
					) );
					foreach ($terms as $t) {
				?>
					<div class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></div>
				<?php 
					} 
				?>
				</div>
				<input type="hidden" name="area_search" id="area_search" value="">
				
			</div>
			<div class="level select-form">
				<div class="title">Cấp học</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="text" value="">-- Chọn cấp học --</div>
				</div>
				<div class="select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'cap-hoc',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<div class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></div>
					<?php 
						} 
					?>
				</div>
				<input type="hidden" name="level" id="level" value="">
				
			</div>
			<div class="career select-form">
				<div class="title">Ngành nghề</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="text" value="">-- Chọn ngành nghề --</div>
				</div>
				<div class="select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'nganh-nghe',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<div class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></div>
					<?php 
						} 
					?>
				</div>
				<input type="hidden" name="career" id="career" value="">
				
			</div>
			<div class="advanced-search-button">
				<button>TÌM NGAY</button>
			</div>
		</form>
	</div>
	
 		<?php
        // Kết thúc nội dung trong widget
 
        echo $after_widget;
	}
}