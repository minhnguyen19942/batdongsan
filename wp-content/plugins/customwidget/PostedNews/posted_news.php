<?php 
/**
*  Quick News Widget
*/
class PostedNews_Widget extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct(
			'postednews_widget',
			'Posted News',

			array(
				'description' => 'Widget Posted News'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'posts_per_page_main' => 8,
			'offset_main' => 0,
			'cat_main' => ''
		);
		$instance = wp_parse_args( (array) $instance, $default);

		$posts_per_page_main = esc_attr( $instance['posts_per_page_main'] );
		$offset_main = esc_attr( $instance['offset_main'] );
		$cat_main = esc_attr( $instance['cat_main'] );

		echo "<h2>Tùy Chỉnh Bài viết</h2>";
		echo "<br>";
		echo "Số bài viết hiển thị: <input class='widefat' type='text' name='".$this->get_field_name('posts_per_page_main')."' value='".$posts_per_page_main."' />";
		echo "Vị trí bài viết thứ mấy: <input class='widefat' type='text' name='".$this->get_field_name('offset_main')."' value='".$offset_main."' />";
		echo "Danh mục của bài viết cần hiển thị ( Để rỗng sẽ lấy tất cả ): <input class='widefat' type='text' name='".$this->get_field_name('cat_main')."' value='".$cat_main."' />";
		
	}

	function update($new_instance, $old_instance){
		parent::update( $new_instance, $old_instance );
 
        $instance = $old_instance;

        $instance['posts_per_page_main'] = strip_tags($new_instance['posts_per_page_main']);
        $instance['offset_main'] = strip_tags($new_instance['offset_main']);
        $instance['cat_main'] = strip_tags($new_instance['cat_main']);


        return $instance;
	}

	function widget($args,$instance){
		$posts_per_page_main = apply_filters( 'widget_posts_per_page_main', $instance['posts_per_page_main'] );
		$offset_main = apply_filters( 'widget_offset_main', $instance['offset_main'] );
		$cat_main = apply_filters( 'widget_cat_main', $instance['cat_main'] );

		extract( $args );
		if($cat_main = ''){
			$args_mainpost = [
				'posts_per_page' => $posts_per_page_main,
				'offset' => $offset_main,
				'post_status' => 'publish',
				'post_type' => 'post'
			];
		} else {
			$args_mainpost = [
				'posts_per_page' => $posts_per_page_main,
				'offset' => $offset_main,
				'cat' => $cat_main,
				'post_status' => 'publish',
				'post_type' => 'post'
			];
		}
		$query_mainpost = new WP_Query($args_mainpost); 
 
        echo $before_widget;
 
 		?>
		<!-- ====================================================== -->
 		
			
			<ul>
			<?php 
				
				
				if($query_mainpost->have_posts()){
					while($query_mainpost->have_posts()){
						$query_mainpost->the_post();
			?>
						<li style="list-style: none;"><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
			<?php 
					}
				}
			?>
			</ul>
		
		<!-- ======================================================= -->
		<?php 
		
        echo $after_widget;
	}

	
	
}