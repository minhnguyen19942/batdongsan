<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'batdongsan');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ND-;f#T=uW=I:@cdP,=i|TrTw#Cf63!m<j<+9wft7bR(5~)Dhr(u ZCw@yynNU==');
define('SECURE_AUTH_KEY',  'gA3=~_i`=F|zt1eEF8-^(Tg-Ej]~v_!V0:T]$STUQ$QcB~i-w;&&pd` I51^y5~#');
define('LOGGED_IN_KEY',    'k`d#!9q@P5N{0iaOJ{!nt>^0U,LU=J/5?zpy6~932jEU$k%oBEzQdAqe3qx<G)Y-');
define('NONCE_KEY',        'M*DQ6Y7=V!5xskkZ$Ge8vHhVs69pXD9>ZUhh,,tmSK.=2?G?$sS$rH!zCj*+M7%5');
define('AUTH_SALT',        'tXnvkch`1SuAZ5?.#a@|A!uYc *|Pl?.@EZ0&R@%U`sO!^[fv8KJ*fLXFUvZ!%]&');
define('SECURE_AUTH_SALT', 'tUoHF@S9XYW y0r ay7LQSg4hOrA,U4W_#]!mo.A_?@M]X5[qH`cED`Gni^sn~b`');
define('LOGGED_IN_SALT',   'rOBZdh`7H/GduJ9&jJj,r={9P@-IzUNVwSrfR==x1AM+.`SZWLPv!uK;+-3U-p3Q');
define('NONCE_SALT',       '<g+bX#=}b>Ls$%d}AWR=z x{ 6[|R9->6k9u_0_HFE>P`:z4&LSZRM(9F W=U`u)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
