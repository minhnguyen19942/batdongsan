<?php 

get_header();
$cat_id = get_the_category()[0]->term_id;
?>


<div class="container">
	<div class="category row no-padding-margin">
		<h3 class="category-title"><?php single_cat_title(); ?></h3>
		<div class="row">
		<?php 
			if(have_posts()):
				while(have_posts()):the_post();
		?>
					<div class="category-item col-sm-3 col-md-3 col-lg-3">
						<a href="<?php the_permalink();?>">
							<?php the_post_thumbnail();?>

							<div class="category-item-info">
								<h3><?php the_title();?></h3>
								<p><?php the_excerpt();?></p>
							</div>
						</a>
					</div>

		<?php 
				endwhile;
				the_posts_pagination( array(
		            'mid_size' => 1,
		            'prev_text' => __( 'Mới nhất', 'minhnguyen' ),
		            'next_text' => __( 'Cũ nhất', 'minhnguyen' ),
		            'screen_reader_text' => ' '
		        ));
				else :
					echo "<h1>Không có kết quả nào được tìm thấy!</h1>";
			endif;
		?>
		</div>
	</div>
</div>	
<?php 

get_footer();
