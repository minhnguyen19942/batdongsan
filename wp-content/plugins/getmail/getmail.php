<?php
/*
Plugin Name: Popup Send Mail
Plugin URI: 
Description: Create some custom widget
Author: Minh Nguyen
Version: 1.0
Author URI: 
*/

function popup_mail() {
    $content = '';
    $form = '
    <style>
        .sendemail-wrapper {
            text-align:center;
        }
        .sendemail-wrapper label {
            font-size:24px;
        }
        .sendemail-content input {
            padding: 10px;
            font-size: 15px;
            font-family: font-normal;
            margin: 10px 0px;
            border: 1px solid #e1e1e1;
            border-radius: 5px; 
            width:100%;
        }
        .sendemail-wrapper input[type=submit] {
            padding: 10px 20px;
            background-color: #e91b22;
            border: 0px;
            border-radius: 10px;
            color: white;
            font-size: 20px;
        }
    </style>
        <button type="button" class="button-sendmail" data-toggle="modal" data-target="#myModal">Vui lòng để lại mail để nhận tin mới nhất!</button>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Gửi email để nhận được thông tin sớm nhất!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="sendemail-wrapper">
                        '.do_shortcode('[contact-form-7 id="151" title="Receive Email"]').'
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>

            </div>
        </div>

    ';

    $js = '
        <script type="text/javascript">
            $(document).ready(function($){
                
            });
        </script>
    ';
    $content = $form.$js;

    return $content;    
 
}
add_shortcode('popup_mail', 'popup_mail');


// <!-- Trigger the modal with a button -->
// <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
