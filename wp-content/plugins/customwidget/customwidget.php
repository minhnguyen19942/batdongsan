<?php
/*
Plugin Name: Custom Widget
Plugin URI: 
Description: Create some custom widget
Author: Minh Nguyen
Version: 1.0
Author URI: 
*/


add_action('widgets_init','create_minhnguyen_widget');

function create_minhnguyen_widget(){
	register_widget('Advance_Search_Widget');
	register_widget('Advance_Search_Mobile_Widget');
	register_widget('Hotnews_Widget');
	register_widget('ContentPages_Widget');
	register_widget('QuickNews_Widget');
	register_widget('PostedNews_Widget');
	register_widget('HeaderNews_Widget');
}

require (plugin_dir_path(__FILE__).'/AdvanceSearch/advance_search_mobile.php');
require (plugin_dir_path(__FILE__).'/AdvanceSearch/advance_search.php');
require (plugin_dir_path(__FILE__).'/AdvanceSearch/taxonomy.php');
require (plugin_dir_path(__FILE__).'/HotNews/hot_news.php');
require (plugin_dir_path(__FILE__).'/ContentPages/content_pages.php');
require (plugin_dir_path(__FILE__).'/QuickNews/quick_news.php');
require (plugin_dir_path(__FILE__).'/PostedNews/posted_news.php');
require (plugin_dir_path(__FILE__).'/HeaderNews/header_news.php');
