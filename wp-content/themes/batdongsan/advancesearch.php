<?php 

/*
Template Name: Advanced Search
*/

if($_GET['area_search'] != ""){
	$area = $_GET['area_search'];
	$param1 = array(
		'taxonomy' => 'khu-vuc',
		'field' => 'term_id',
		'terms' => $area
	);
}
if($_GET['level'] != ""){
	$level = $_GET['level'];
	$param2 = array(
		'taxonomy' => 'cap-hoc',
		'field' => 'term_id',
		'terms' => $level
	);
}
if($_GET['career'] != ""){
	$career = $_GET['career'];
	$param3 = array(
		'taxonomy' => 'nganh-nghe',
		'field' => 'term_id',
		'terms' => $career
	);
}
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = [
	'post_type' => 'post',
	'posts_per_page' => 4,
	'paged'=>$paged,
	'tax_query' => array(
		$param1,
		$param2,
		$param3
	),
];

$query = new WP_Query($args);

get_header();
?>
<?php looping_text();?>
<div class="as-query row no-padding-margin">
	<div class="col-sm-9 col-md-9">
		<h3>Kết quả tìm kiếm: </h3>
		<?php 
			if($query->have_posts()):
				while($query->have_posts()):$query->the_post();
		?>
					<div class="as-query-item">
						<a href="<?php the_permalink();?>">
							<?php the_post_thumbnail();?>
							<div class="as-query-item-info">
								<h1><?php the_title();?></h1>
								<p><?php the_excerpt();?></p>
							</div>
						</a>
					</div>
		<?php 
				endwhile;
				$GLOBALS['wp_query']->max_num_pages = $query->max_num_pages;
				the_posts_pagination( array(
		            'mid_size' => 1,
		            'prev_text' => __( 'Mới nhất', 'minhnguyen' ),
		            'next_text' => __( 'Cũ nhất', 'minhnguyen' ),
		            'screen_reader_text' => ' '
		        ));
				else :
					echo "<h1>Không có kết quả nào được tìm thấy!</h1>";
			endif;
		?>
	</div>
	<div class="col-sm-3 col-md-3">
		<?php 
			if(is_active_sidebar('advanced-search')){
				dynamic_sidebar('advanced-search');
			}
		?>
	</div>
</div>

<?php 
get_footer();

