<?php 

get_header();
?>
<div class="container">
	<div class="as-query row no-padding-margin">
		<div class="col-sm-9 col-md-9">
			<h3 class="as-query-title">Kết quả tìm kiếm:</h3>
			<div class="row">
			<?php 
				if(have_posts()):
					while(have_posts()):the_post();
			?>
						<div class="as-query-item col-sm-4 col-md-4 col-lg-4">
							<a href="<?php the_permalink();?>">
								<?php the_post_thumbnail();?>

								<div class="as-query-item-info">
									<h3><?php the_title();?></h3>
									<p><?php the_excerpt();?></p>
								</div>
							</a>
						</div>

			<?php 
					endwhile;
					the_posts_pagination( array(
			            'mid_size' => 1,
			            'prev_text' => __( 'Mới nhất', 'minhnguyen' ),
			            'next_text' => __( 'Cũ nhất', 'minhnguyen' ),
			            'screen_reader_text' => ' '
			        ));
					else :
						echo "<h1>Không có kết quả nào được tìm thấy!</h1>";
				endif;
			?>
			</div>
		</div>
		<div class="col-sm-3 col-md-3">
			<?php 
				
			?>
		</div>
	</div>
</div>
<?php 
get_footer();

