<?php
/**
*  Advance Search Widget
*/
class Advance_Search_Mobile_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct(
			'advance_search_mobile_widget',
			'Widget Advance Search Mobile',

			array(
				'description' => 'Widget Advance Search Mobile By Taxanomy'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'title' => 'Advance Search Mobile Widget'
		);
	}

	function update($new_instance, $old_instance){

	}
	function widget($args,$instance){
		extract( $args );
 
        echo $before_widget;
 
        //In tiêu đề widget
        //echo $before_title.$title.$after_title;
 
        // Nội dung trong widget
 	?>
	<script type="text/javascript">
	
		$(document).ready(function($){
			area_mobile = $(".area-search-mobile .select");
			level_mobile = $(".level-mobile .select");
			career_mobile = $(".career .select");

			// ==========================================================
			$(".area-search-mobile .text").click(function(e){
				if(area_mobile.hasClass("active")){
					area_mobile.removeClass("active");
					area_mobile.slideUp(200);
				} else {
					area_mobile.addClass("active");
					level_mobile.slideUp(200);
					career_mobile.slideUp(200);
					area_mobile.slideDown(200);
				}
			});
			$(".area-search-mobile .option").click(function(e){
				valueOptionMobile = $(this).attr("value");
				htmlOptionMobile =  $(this).html();
				$("#area_search_mobile").val(valueOptionMobile);
				$(".area-search-mobile .text").html(htmlOptionMobile);
				area_mobile.removeClass("active");
				area_mobile.slideUp(200);
			});


			// ==========================================================			
			$(".level-mobile .text").click(function(e){
				if(level_mobile.hasClass("active")){
					level_mobile.removeClass("active");
					level_mobile.slideUp(200);
				} else {
					level_mobile.addClass("active");
					area_mobile.slideUp(200);
					career_mobile.slideUp(200);
					level_mobile.slideDown(200);
				}
			});
			$(".level-mobile .option").click(function(e){
				valueOptionMobile = $(this).attr("value");
				htmlOptionMobile =  $(this).html();
				$("#level_mobile").val(valueOptionMobile);
				$(".level-mobile .text").html(htmlOptionMobile);
				level_mobile.removeClass("active");
				level_mobile.slideUp(200);
			});

			// ==========================================================
			$(".career-mobile .text").click(function(e){
				if(career_mobile.hasClass("active")){
					career_mobile.removeClass("active");
					career_mobile.slideUp(200);
				} else {
					career_mobile.addClass("active");
					area_mobile.slideUp(200);
					level_mobile.slideUp(200);
					career_mobile.slideDown(200);
				}
			});
			$(".career-mobile .option").click(function(e){
				valueOptionMobile = $(this).attr("value");
				htmlOptionMobile =  $(this).html();
				$("#career_mobile").val(valueOptionMobile);
				$(".career-mobile .text").html(htmlOptionMobile);
				career_mobile.removeClass("active");
				career_mobile.slideUp(200);
			});


			// ==========================================================
			$(".select-form .get_value").click(function(e){
				e.stopPropagation();
			});

		});



		// ==========================================================
		$(document).click(function(){
			if($(".select-form .select").hasClass("active")){
				area_mobile.removeClass("active");
				area_mobile.slideUp(200);
		    	level_mobile.removeClass("active");
				level_mobile.slideUp(200);
		    	career_mobile.removeClass("active");
				career_mobile.slideUp(200);
		    }
		});
	</script>
<div class="advanced-search-mobile">
 	<div class="advanced-search">
        <form action="<?php echo esc_url( 'http://gochamhoc.dev/advance-search/' ); ?>" method="GET">
			<h3>TÌM KIẾM NÂNG CAO</h3>
			<div class="area-search-mobile select-form">
				<div class="title">Khu vực</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="text" value="">-- Chọn khu vực --</div>
				</div>
				<div class="select">
				<?php
					$terms = get_terms( array(
					    'taxonomy' => 'khu-vuc',
					    'hide_empty' => false,
					    'parent' => 0
					) );
					foreach ($terms as $t) {
				?>
					<div class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></div>
				<?php 
					} 
				?>
				</div>
				<input type="hidden" name="area_search" id="area_search_mobile" value="">
				<script type="text/javascript">
					$(document).ready(function($){
						
					});
				</script>
			</div>
			<div class="level-mobile select-form">
				<div class="title">Cấp học</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="text" value="">-- Chọn cấp học --</div>
				</div>
				<div class="select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'cap-hoc',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<div class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></div>
					<?php 
						} 
					?>
				</div>
				<input type="hidden" name="level" id="level_mobile" value="">
				<script type="text/javascript">
					
				</script>
			</div>
			<div class="career select-form">
				<div class="title">Ngành nghề</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="text" value="">-- Chọn ngành nghề --</div>
				</div>
				<div class="select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'nganh-nghe',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<div class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></div>
					<?php 
						} 
					?>
				</div>
				<input type="hidden" name="career" id="career_mobile" value="">
				<script type="text/javascript">
					$(document).ready(function($){
						
					});
				</script>
			</div>
			<div class="advanced-search-button">
				<button>TÌM NGAY</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function($){
		
	});
	
</script>
 		<?php
        // Kết thúc nội dung trong widget
 
        echo $after_widget;
	}
}