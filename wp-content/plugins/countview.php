<?php
/*
Plugin Name: Count View
Version: 1.0
Author: Minh Nguyễn
*/

global $jal_db_version;
$jal_db_version = '1.0';

function jal_install() {
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'countview';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id bigint(20) NOT NULL AUTO_INCREMENT,
		post_id bigint(20) NOT NULL,
		timeview datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		view bigint NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}

function delete_table(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'countview';
	$sql = $wpdb->prepare("DROP TABLE IF EXISTS $table_name");
	$query = $wpdb->query($sql);
	delete_option("my_plugin_db_version");
}

register_activation_hook( __FILE__, 'jal_install' );
register_deactivation_hook(__FILE__, 'delete_table'); 

function add_countview($postId, $view, $time){// Tạo thêm dữ liệu đếm theo bài post
	global $wpdb;
	$wpdb->insert(
		$wpdb->prefix.'countview',
		[
			'post_id' => $postId,
			'view' => $view,
			'timeview' => $time
		],
		[
			'%d',
			'%d',
			'%s'
		]
	);
}

function update_countview($postId, $view, $time){// Cập nhật thêm dữ liệu đếm theo bài post

	global $wpdb;
	$wpdb->update(
		$wpdb->prefix.'countview',
		[
			'view' => $view
		],
		[
			'post_id' => $postId,
			'timeview' => $time
		],
		[
			'%d'
		],
		[
			'%d',
			'%s'
		]
	);
}

function delete_countview(){// Xóa dữ liệu theo bài post
	global $wpdb;
	$table_name = $wpdb->prefix.'countview';
	$sql = $wpdb->prepare("DELETE * FROM $table_name");
	$query = $wpdb->query($sql);
	return $query;
}

function checkIfHasID($postId,$time){// Kiểm tra xem đã tồn tại post này vào thời điểm ngày hiện tại hay chưa

	global $wpdb;
	$table_name = $wpdb->prefix.'countview';
	// lấy ra số lượng post id
	$sql_count = $wpdb->prepare("SELECT COUNT(post_id) FROM $table_name WHERE post_id = %d",$postId);
	$query_count = $wpdb->get_var($sql_count);
	// lấy ra thời gian theo post id
	$sql_time = $wpdb->prepare("SELECT timeview FROM $table_name WHERE post_id = %s",$postId);
	$query_time = $wpdb->get_results($sql_time,ARRAY_A);
	// so sánh thời gian

	$time_get = [];// thời gian lấy ra
	$time_current = strtotime($time);// thời gian hiện tại
	foreach ($query_time as $t) {
		$time_get[] = strtotime($t['timeview']);
	}

	if($query_count >= 1 && in_array($time_current, $time_get)){
		return true;
	} else {
		return false;
	}
}

function getView($postId, $time){// Lấy ra view theo id post và thời gian
	global $wpdb;
	$table_name = $wpdb->prefix.'countview';
	$sql = $wpdb->prepare("SELECT view FROM $table_name WHERE post_id = %d AND timeview = %s",$postId, $time);
	$query = $wpdb->get_var($sql);
	return $query;
}

function setPostView($postId, $time){// Thiết lập view cho post : nếu chưa có thì tạo, có rồi thì cập nhật laị view
	if(date("Y-m-d H:i:s") == date("Y-m-01 00:00:00")){
		delete_countview();
	}

	if(checkIfHasID($postId,$time)){
		$view = getView($postId,$time);
		$newView = $view + 1;
		update_countview($postId,$newView, $time);
	} else {
		add_countview($postId,1,$time);
	}
}

function getPostView($postId, $time){// lấy ra view của post
	if(checkIfHasID($postId,$time)){
		return getView($postId,$time);
	}
}


function getPostByDay($time,$cat_id){
	global $wpdb;

	$table_countview = $wpdb->prefix.'countview';
	$table_term_relationships = $wpdb->prefix.'term_relationships';
	
	$sql_term_relationships = $wpdb->prepare("SELECT object_id FROM $table_term_relationships WHERE term_taxonomy_id = %d",$cat_id);

	$sql_countview = $wpdb->prepare("SELECT post_id FROM $table_countview WHERE timeview = %s ORDER BY view DESC limit 6",$time);
	
	$results_countviews = $wpdb->get_results($sql_countview,ARRAY_A);
	
	$results_term_relationships = $wpdb->get_results($sql_term_relationships,ARRAY_A);

	$query_countview = [];
	foreach ($results_countviews as $rc) {
		$query_countview[] = implode("",$rc);
	}
	$query_term_relationships = [];
	foreach ($results_term_relationships as $rr) {
		$query_term_relationships[] = implode("",$rr);
	}
	$query = array_intersect($query_countview, $query_term_relationships);

	return $query;
}


function getPostByMonth($cat_id){
	global $wpdb;

	$table_countview = $wpdb->prefix.'countview';
	$table_term_relationships = $wpdb->prefix.'term_relationships';
	
	$sql_term_relationships = $wpdb->prepare("SELECT object_id FROM $table_term_relationships WHERE term_taxonomy_id = %d",$cat_id);

	$sql_countview = $wpdb->prepare("SELECT post_id FROM $table_countview GROUP BY post_id ORDER BY SUM(view) DESC limit 6",$time);

	$results_countviews = $wpdb->get_results($sql_countview,ARRAY_A);
	$results_term_relationships = $wpdb->get_results($sql_term_relationships,ARRAY_A);

	$query_countview = [];
	foreach ($results_countviews as $rc) {
		$query_countview[] = implode("",$rc);
	}
	$query_term_relationships = [];
	foreach ($results_term_relationships as $rr) {
		$query_term_relationships[] = implode("",$rr);
	}
	$query = array_intersect($query_countview, $query_term_relationships);

	return $query;
}
// function getPostView($postId){
// 	$count_key = 'count_view';
// 	$count_view =  get_post_meta($postId,$count_key,true);
// 	if($count_view == ''){
// 		delete_post_meta($postId, $count_key);
// 		add_post_meta($postId, $count_key, 0);
// 		return "0";
// 	} 
// 	return $count_view[0];
// }

// function setPostView($postId){
// 	$count_key = 'count_view';
// 	$count_view = get_post_meta($postId,$count_key,true);

// 	if($count_view === ''){
// 		delete_post_meta($postId, $count_key);
// 		add_post_meta($postId, $count_key, 0);
// 	} else {
// 		$count_view++;
// 		update_post_meta($postId, $count_key, $count_view);
// 	}
// }

// ?>