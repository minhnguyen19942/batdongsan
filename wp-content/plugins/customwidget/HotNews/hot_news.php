<?php 
/**
*  Hotnews Widget
*/
class Hotnews_Widget extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct(
			'hotnews_widget',
			'Hot News Search',

			array(
				'description' => 'Widget Hotnews'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'title' => 'Widget Hotnews Widget'
		);

	}

	function update($new_instance, $old_instance){

	}

	function widget($args,$instance){
		extract( $args );
 
        echo $before_widget;
 
        //In tiêu đề widget
        //echo $before_title.$title.$after_title;
 
        // Nội dung trong widget
 		?>
 	
 		<?php
        // Kết thúc nội dung trong widget
 
        echo $after_widget;
	}

	function minhnguyen_set_post_views($postID) {
	    $count_key = 'count_view';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    }else{
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
	
}