<?php 
/*
Template Name: Category
*/
get_header();
?>

		<!-- Category Top -->
		<div class="category-top">
			<!-- category -->
			<div class="category">
				<div class="category-title-top">
					<h4>TUYỂN SINH<div class="triangle_right_category"></div></h4>
					<div class="clear-fix"></div>
				</div>
				<div class="category-body row no-padding-margin">
					<div class="category-news col-md-9 col-sm-9">
						<div class="category-news-content row no-padding-margin">
							<div class="category-news-content-mainnews">
								<div class="category-mainnews-thumbnail">
									<img src="<?php echo get_template_directory_uri()?>/assets/images/banner-main.png">
								</div>
								<div class="category-mainnews-content">
									<h1>Du học là 1 lựa chọn đúng đắn tối ưu?</h1>
									<p>Công ty du học việt nam có rất nhiều, các bậc phụ huynh và các bạn cần tìm hiểu kỹ các thông tin về các nước mà bạn sẽ đi du học, các trường mà các bạn dự tính học tập.</p>
									<ul>
										<li><a href="#"><div class="another-news">Cơ hội định cư dễ dàng chỉ sau 1 năm học</div></a></li>
										<li><a href="#"><div class="another-news">Cơ hội định cư dễ dàng chỉ sau 1 năm học</div></a></li>
										<li><a href="#"><div class="another-news">Cơ hội định cư dễ dàng chỉ sau 1 năm học</div></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="category-hotnews-inday">
							<div class="title-in">
								<h3>TIÊU ĐIỂM TRONG NGÀY</h3>
								<hr>
							</div>
							<div class="row no-padding-margin">
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
							</div>
							<div class="divide-cate-hotnews"><hr></div>
							<div class="row no-padding-margin">
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
							</div>
						</div>
						<div class="category-mostview-inmoth">
							<div class="title-in">
								<h3>ĐỌC NHIỀU TRONG THÁNG</h3>
								<hr>
							</div>
							<div class="row no-padding-margin">
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
							</div>
							<div class="divide-cate-hotnews"><hr></div>
							<div class="row no-padding-margin">
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
								<div class="wrapper col-md-4 col-sm-4">
									<a href="#">	
										<img src="<?php echo get_template_directory_uri()?>/assets/images/tintuyendung.png" alt="">
										<h3>Săn học bổng 100% năm 2017</h3>
										<p>Du học là quyết định đầu tư không nhỏ cả về thời gian kinh phí</p>
									</a>
								</div>
							</div>
						</div>
						<div class="category-ads">
							<a href="#"><img src="<?php echo get_template_directory_uri()?>/assets/images/banner-bottom.png"></a>
						</div>
						<div class="category-old-posts">
							<h4>
								<div><img src="<?php echo get_template_directory_uri()?>/assets/images/triangle_icon.png" alt=""></div>TIN ĐÃ ĐĂNG
							</h4>
							<ul>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
								<li><a href="#">Cơ hội định cư dễ dàng chỉ sau 1 năm học</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 col-sm-3">
						<div class="category-hotnews">
							<h4>TÂM ĐIỂM</h4>
							<div class="owl-carousel">
								<div class="item">

									<a href="#">
										<div class="item-wrapper">
											<img src="<?php echo get_template_directory_uri()?>/assets/images/thumbnail-tamdiem.png">
											<div class="item-content">
												<h2>Hội đồng anh trao giải cao</h2>
												<p>GSE-beo tự hào được Hôị Đồng Anh trao giải thưởng là một trong những công ty tư vấn du học hàng đầu việt nam.</p>
											</div>
										</div>
									</a>
									<a href="#">
										<div class="item-wrapper">
											<img src="<?php echo get_template_directory_uri()?>/assets/images/thumbnail-tamdiem.png">

											<div class="item-content">
												<h2>Hội đồng anh trao giải cao</h2>
												<p>GSE-beo tự hào được Hôị Đồng Anh trao giải thưởng là một trong những công ty tư vấn du học hàng đầu việt nam.</p>
											</div>
										</div>
									</a>
									<a href="#">
										<div class="item-wrapper">
											<img src="<?php echo get_template_directory_uri()?>/assets/images/thumbnail-tamdiem.png">

											<div class="item-content">
												<h2>Hội đồng anh trao giải cao</h2>
												<p>GSE-beo tự hào được Hôị Đồng Anh trao giải thưởng là một trong những công ty tư vấn du học hàng đầu việt nam.</p>
											</div>
										</div>
									</a>
									
								</div>
								<div class="item">
									<a href="#">
										<div class="item-wrapper">
											<img src="#">
											<div class="item-content">
												<h2>Hội đồng anh trao giải cao</h2>
												<p>GSE-beo tự hào được Hôị Đồng Anh trao giải thưởng là một trong những công ty tư vấn du học hàng đầu việt nam.</p>
											</div>
										</div>
									</a>
								</div>
								
							</div>
						</div>
						<!-- Advanced Search -->
						<div class="advanced-search">
							<form action="#" method="POST">
								<h3>TÌM KIẾM NÂNG CAO</h3>
								<div class="area-search select-form">
									<div class="title">Khu vực</div>
									<div class="text" value="0">Hoàng Mai</div>
									<div class="select">
										<div class="option" value="0">Hoàng Mai</div>
										<div class="option" value="1">Đống Đa</div>
										<div class="option" value="2">Hai Bà Trưng</div>
										<div class="option" value="3">Hà Đông</div>
									</div>
									<input type="hidden" name="area_search" id="area_search" value="0">
									<script type="text/javascript">
										$(document).ready(function($){
											select = $(".select-form .select");
											$(".select-form .text").click(function(e){
												if(select.hasClass("active")){
													select.removeClass("active");
													select.slideUp(200);
												} else {
													select.addClass("active")
													select.slideDown(200);
												}
											});
											$(".option").click(function(e){
												valueOption = $(this).attr("value");
												htmlOption =  $(this).html();
												$("#area_search").val(valueOption);
												$(".text").html(htmlOption);
												select.removeClass("active");
												select.slideUp(200);
											});
										});
									</script>
									
								</div>
								
								
								<div class="advanced-search-button">
									<button>TÌM NGAY</button>
								</div>
							</form>
						</div>
						<!-- Advanced Search -->
					
					</div>
				</div>
			</div>
			<!-- category -->
		</div>
		<!-- Category Top -->

		
<?php 

get_footer();
