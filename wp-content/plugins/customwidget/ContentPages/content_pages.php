<?php 
/**
*  Content Pages Widget
*/
class ContentPages_Widget extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct(
			'contentpages_widget',
			'Content Pages',

			array(
				'description' => 'Widget ContentPages'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'posts_per_page_main' => 1,
			'offset_main' => 0,
			'cat_main' => 4,
			'posts_per_page_sub' => 3,
			'offset_main_sub' => 1,
			'cat_main_sub' => 4
		);
		$instance = wp_parse_args( (array) $instance, $default);

		$posts_per_page_main = esc_attr( $instance['posts_per_page_main'] );
		$offset_main = esc_attr( $instance['offset_main'] );
		$cat_main = esc_attr( $instance['cat_main'] );

		$posts_per_page_sub = esc_attr( $instance['posts_per_page_sub'] );
		$offset_main_sub = esc_attr( $instance['offset_main_sub'] );
		$cat_main_sub = esc_attr( $instance['cat_main_sub'] );
		echo "<h2>Bài viết chính</h2>";
		echo "<br>";
		echo "Số bài viết hiển thị: <input class='widefat' type='text' name='".$this->get_field_name('posts_per_page_main')."' value='".$posts_per_page_main."' />";
		echo "Vị trí bài viết thứ mấy: <input class='widefat' type='text' name='".$this->get_field_name('offset_main')."' value='".$offset_main."' />";
		echo "Danh mục của bài viết cần hiển thị: <input class='widefat' type='text' name='".$this->get_field_name('cat_main')."' value='".$cat_main."' />";
		echo "<h2>Các bài viết phụ</h2>";
		echo "<br>";
		echo "Số bài viết hiển thị: <input class='widefat' type='text' name='".$this->get_field_name('posts_per_page_sub')."' value='".$posts_per_page_sub."' />";
		echo "Vị trí bài viết thứ mấy: <input class='widefat' type='text' name='".$this->get_field_name('offset_main_sub')."' value='".$offset_main_sub."' />";
		echo "Danh mục của bài viết cần hiển thị: <input class='widefat' type='text' name='".$this->get_field_name('cat_main_sub')."' value='".$cat_main_sub."' />";
	}

	function update($new_instance, $old_instance){
		parent::update( $new_instance, $old_instance );
 
        $instance = $old_instance;

        $instance['posts_per_page_main'] = strip_tags($new_instance['posts_per_page_main']);
        $instance['offset_main'] = strip_tags($new_instance['offset_main']);
        $instance['cat_main'] = strip_tags($new_instance['cat_main']);

        $instance['posts_per_page_sub'] = strip_tags($new_instance['posts_per_page_sub']);
        $instance['offset_main_sub'] = strip_tags($new_instance['offset_main_sub']);
        $instance['cat_main_sub'] = strip_tags($new_instance['cat_main_sub']);

        return $instance;
	}

	function widget($args,$instance){
		$posts_per_page_main = apply_filters( 'widget_posts_per_page_main', $instance['posts_per_page_main'] );
		$offset_main = apply_filters( 'widget_offset_main', $instance['offset_main'] );
		$cat_main = apply_filters( 'widget_cat_main', $instance['cat_main'] );

		$posts_per_page_sub = apply_filters( 'widget_posts_per_page_sub', $instance['posts_per_page_sub'] );
		$offset_main_sub = apply_filters( 'widget_offset_main_sub', $instance['offset_main_sub'] );
		$cat_main_sub = apply_filters( 'widget_cat_main_sub', $instance['cat_main_sub'] );
		extract( $args );

		$args_mainpost = [
			'posts_per_page' => $posts_per_page_main,
			'offset' => $offset_main,
			'cat' => $cat_main,
			'post_status' => 'publish',
			'post_type' => 'post'
		];
		$query_mainpost = new WP_Query($args_mainpost); 
 
        echo $before_widget;
 
        if($query_mainpost->have_posts()){
			while($query_mainpost->have_posts()){
				$query_mainpost->the_post();
 		?>
 			
 		<div class="education-mainnews-thumbnail">
			<a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
		</div>
		<div class="education-mainnews-content">
			<h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
			<p><?php the_excerpt();?></p>
		</div>

		<?php
			}
		}


		$args_subpost = [
			'posts_per_page' => $posts_per_page_sub,
			'offset' => $offset_main_sub,
			'cat' => $cat_main_sub,
			'post_status' => 'publish',
			'post_type' => 'post'
		];
		$query_subpost = new WP_Query($args_subpost); 
		if($query_subpost->have_posts()){
			while($query_subpost->have_posts()){
				$query_subpost->the_post();
 		?>
			<li><a href="<?php echo the_permalink();?>"><div class="another-news"><?php the_title();?></div></a></li>
 		<?php 
			}
		}
        echo $after_widget;
	}

	
	
}