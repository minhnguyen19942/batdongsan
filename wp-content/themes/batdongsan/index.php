<?php 
/*
Home page
*/
get_header();
?>
	<div class="slider">
	    <!-- Slider -->
			<?php echo do_shortcode('[metaslider id="5"]'); ?>
	    <!-- Slider -->
  	</div>
	  <div class="container main-page">
	    <!-- Section 1 -->
	    <div class="section section-1">
	      <h2 class="section-1_title">
	        DỰ ÁN
	      </h2>
	      <hr>
	      <div class="section-1_list row">
	      	<?php 
				$args = [
					'posts_per_page' => 6,
					'offset' => 0,
					// 'cat' => 1,
					'post_status' => 'publish',
					'post_type' => 'post'
				];
				$query = new WP_Query($args); 
			?>
			
			<?php 
				if($query->have_posts()){
					while($query->have_posts()){
						$query->the_post();
			?>
			<div class="col-sm-3 col-md-3 col-lg-3 animated fadeInUp">
	          <div class="section-1_list--thumbnail">
	            <a href="<?php echo the_permalink();?>">
	            	<!-- <img src="img/thumbnail.jpg" class="hover-blur thumbnail"> -->
	            	<?php echo get_the_post_thumbnail();?>
	            </a>
	          </div>
	          <h3 class="section-1_list--title"><a href="<?php echo the_permalink();?>"><?php echo get_the_title();?></a></h3>
	        </div>	
			<?php 
					}
				}
			?>

	        <!-- Line 2 -->
	      </div>
	    </div>
	    <!-- Section 1 -->

	    <!-- Section 2 -->
	    <div class="section section-2">
	      <h2 class="section-2_title">TIN TỨC CẬP NHẬT MỚI NHẤT</h2>
	      <hr>
	      <div class="section-2_wrapper row">
	        <div class="section-2_list col-sm-8 col-md-8 col-lg-8 animated fadeInUp">
        	<?php 
				$args = [
					'posts_per_page' => 3,
					'offset' => 0,
					// 'cat' => 1,
					'post_status' => 'publish',
					'post_type' => 'post'
				];
				$query = new WP_Query($args);
			?>
			<?php 
				if($query->have_posts()){
					while($query->have_posts()){
						$query->the_post();
			?>
	          <div class="section-2_list--item row">
	            <div class="section-2_list--item---thumbnail col-sm-3 col-md-3 col-lg-3">
	              <a href="<?php echo the_permalink();?>">
	              	<?php echo get_the_post_thumbnail();?>
	              </a>
	            </div>
	            <div class="section-2_list--item---text col-sm-9 col-md-9 col-lg-9">
	              <h4 class="section-2_list--item---text----title">
	                <a href="<?php echo the_permalink();?>"><?php echo get_the_title();?></a>
	              </h4>
	              <p class="section-2_list--item---text----content"><?php the_excerpt();?></p>
	            </div>
	          </div>  
          	<?php 
					}
				}
			?>

	        </div>
	        <div class="section-2_banner col-sm-4 col-md-4 col-lg-4">
	        	<?php 
      				if(is_active_sidebar("right-banner")){
      					dynamic_sidebar("right-banner");
      				}
      			?>
	        </div>
	      </div>
	    </div>
	    <!-- Section 2 -->

	    <!-- Section 3 -->
	    <div class="section section-3">
	      <h2 class="section-3_title">TIN TỨC</h2>
	      <hr>
	      <div class="section-3_list row">
			<?php 
				$args = [
					'posts_per_page' => 3,
					'offset' => 0,
					// 'cat' => 1,
					'post_status' => 'publish',
					'post_type' => 'post'
				];
				$query = new WP_Query($args);
			?>
			<?php 
				if($query->have_posts()){
					while($query->have_posts()){
						$query->the_post();
			?>
	        <div class="section-3_list--item col-sm-4 col-md-4 col-lg-4">
	          <div class="section-3_list--item---thumbnail">
	            <a href="<?php echo the_permalink();?>"><?php echo get_the_post_thumbnail();?></a>
	          </div>
	          <h3 class="section-3_list--item---title"><a href="<?php echo the_permalink();?>"><?php echo get_the_title();?></a></h3>
	          <div class="section-3_list--item---content"><?php the_excerpt();?></div>          
	        </div>
	        <?php 
					}
				}
			?>
	       
	      </div>
	    </div>
	    <!-- Section 3 -->
	  </div>

<?php 
get_footer();