<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

?>
<style type="text/css">
	
</style>


<div class="container wrapper-single-post">
	<div class="breadcumb">
		<?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?>
	</div>

	<?php 
		if(have_posts()):
			while(have_posts()) : the_post();
				get_template_part('content','single');
			endwhile;
		endif;
	?>
	<div class="related-single-post">
		<h3>Bài viết liên quan:</h3>
			
	</div>
</div>
<?php get_footer();
