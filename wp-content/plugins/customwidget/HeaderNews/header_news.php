<?php 
/**
*  Header News Widget
*/
class HeaderNews_Widget extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct(
			'headernews_widget',
			'Header News',

			array(
				'description' => 'Widget Header News'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'title' => "",
			'link' => "",
			'color' => "",
			
		);
		$instance = wp_parse_args( (array) $instance, $default);

		$title = esc_attr( $instance['title'] );
		$link = esc_attr( $instance['link'] );
		$color = esc_attr( $instance['color'] );

		echo "<h2>Cấu hình Header</h2>";
		echo "<br>";
		echo "Tiêu đề: <input class='widefat' type='text' name='".$this->get_field_name('title')."' value='".$title."' />";
		echo "Link: <input class='widefat' type='text' name='".$this->get_field_name('link')."' value='".$link."' />";
		echo "Mã Màu: <input class='widefat' type='text' name='".$this->get_field_name('color')."' value='".$color."' />";
		
	}

	function update($new_instance, $old_instance){
		parent::update( $new_instance, $old_instance );
 
        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['link'] = strip_tags($new_instance['link']);
        $instance['color'] = strip_tags($new_instance['color']);

        return $instance;
	}

	function widget($args,$instance){
		$title = apply_filters( 'widget_title', $instance['title'] );
		$link = apply_filters( 'widget_link', $instance['link'] );
		$color = apply_filters( 'widget_color', $instance['color'] );

		extract( $args );

 
        echo $before_widget;
        ?>
 			<div class="admission-title-top" style="border-bottom:2px solid <?php echo $color;?>">
				<h4 style="background-color:<?php echo $color;?>"><a href="<?php echo $link;?>"><?php echo $title;?></a>
					<div class="triangle_right_admission" style="border-left:20px solid <?php echo $color;?>"></div>
				</h4>
				<div class="clear-fix"></div>
			</div>
        <?php
        echo $after_widget;
	}

	
	
}