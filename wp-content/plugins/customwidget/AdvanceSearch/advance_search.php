<?php
/**
*  Advance Search Widget
*/
class Advance_Search_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct(
			'advance_search_widget',
			'Widget Advance Search',

			array(
				'description' => 'Widget Advance Search By Taxanomy'
			)
		);
	}

	function form($instance){
		parent::form($instance);
		$default = array(
			'title' => 'Advance Search Widget'
		);
	}

	function update($new_instance, $old_instance){

	}
	function widget($args,$instance){
		extract( $args );
 
        echo $before_widget;
 
        //In tiêu đề widget
        //echo $before_title.$title.$after_title;
 
        // Nội dung trong widget
 	?>
<script type="text/javascript">	
$(function() {

	$("#area_search").customselect();
	$("#level").customselect();
	$("#career").customselect();

});
</script>

 	<div class="advanced-search">
        <form action="<?php echo htmlspecialchars(esc_url( bloginfo('url').'/advance-search/' )); ?>" method="GET" name="advanced-search-form" id="advanced-search-form">
			<h3>TÌM KIẾM NÂNG CAO</h3>
			<div class="area-search select-form">
				<div class="title">Khu vực</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					
				</div>
				<select id="area_search" name="area_search" class="select custom-select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'khu-vuc',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<option class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></option>
					<?php 
						} 
					?>
				</select>
				
				
			</div>
			<div class="level select-form">
				<div class="title">Cấp học</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
				</div>
				<select id="level" name="level" class="select custom-select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'cap-hoc',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<option class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></option>

					<?php 
						} 
					?>
				</select>
				
			</div>
			<div class="career select-form">
				<div class="title">Ngành nghề</div>
				<div class="get_value">
					<div class="icon">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
				</div>
				<select id="career" name="career" class="select custom-select">
					<?php
						$terms = get_terms( array(
						    'taxonomy' => 'nganh-nghe',
						    'hide_empty' => false,
						    'parent' => 0
						) );
						foreach ($terms as $t) {
					?>
						<option class="option" value="<?php echo $t->term_id;?>"><?php echo $t->name;?></option>
					<?php 
						} 
					?>
				</select>
				
			</div>
			<div class="advanced-search-button">
				<button>TÌM NGAY</button>
			</div>
		</form>
	</div>
	
 		<?php
        // Kết thúc nội dung trong widget
 
        echo $after_widget;
	}
}