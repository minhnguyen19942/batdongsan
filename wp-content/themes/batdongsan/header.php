<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/animate.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/lightslider.css">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/lightslider.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(window).scroll(function (event) {
			var scroll = $(window).scrollTop();
			if(scroll == 0){
				$("#header").removeClass("header-active");
				$("#header").removeClass("headerbox_active");
				$("#header .container").removeClass("header-active");
				$("#header .container .row").removeClass("header-active");
				$("#logo-img").removeClass("header-active");
				$(".menu-header").removeClass("menu-header-active");
				$(".search-header").removeClass("search-header-active");
			} else {
				$("#header").addClass("header-active");
				$("#header").addClass("headerbox_active");
				$("#header .container").addClass("header-active");
				$("#header .container .row").addClass("header-active");
				$("#logo-img").addClass("header-active");
				$(".menu-header").addClass("menu-header-active");
				$(".search-header").addClass("search-header-active");
			}  
		});
		$('#lightSlider').lightSlider({
		    gallery: true,
		    item: 1,
		    loop:true,
		    slideMargin: 0,
		    thumbItem: 9
		});	
	});
	</script>
	<?php wp_head(); ?>
</head>

<body>
<div class="wrapper-all">
  <header class="header" id="header">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-3 logo-header">
          <a href="#">
            <div id="logo-img" class="deactive" style="background: url(<?php echo get_template_directory_uri();?>/img/logo.png) no-repeat left center; background-size: auto 100%;"></div>
          </a>
        </div>
        <div class="col-sm-7 col-md-7 col-lg-7 menu-header">
          <ul>
            <li>
              <a href="#"><div class="menu-item">Menu 1</div></a>
              <ul class="sub-menu">
                <li><a href="#"><div class="menu-item">Menu 1</div></a></li>                
                <li><a href="#"><div class="menu-item">Menu 1</div></a></li>                
                <li><a href="#"><div class="menu-item">Menu 1</div></a></li>                
              </ul>
            </li>
            <li><a href="#"><div class="menu-item">Menu 1</div></a></li>
            <li><a href="#"><div class="menu-item">Menu 1</div></a></li>
            <li><a href="#"><div class="menu-item">Menu 1</div></a></li>
            <li><a href="#"><div class="menu-item">Menu 1</div></a></li>
          </ul>
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2 search-header">
          <?php echo get_search_form();?>
        </div>
      </div>
    </div>
  </header>
